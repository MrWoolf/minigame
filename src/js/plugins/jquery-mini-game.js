(function( $ ) {
    $.fn.miniGame = function() {

        let globalIndexWin = 0,
            globalIndexLose = 0;

        let fn = {
            init($plugin, form, time){
                let $form = $(form),
                    startBtn = $form.find('.btn-start'),
                    timeOut = time[0].value,
                    index = 1,
                    interval,
                    randomArray = this.getRandomArray(),
                    $resumeModalWindow = $plugin.find('.result-game');

                globalIndexWin = 0;
                globalIndexLose = 0;

                this.updatePCCoutScore($plugin, globalIndexLose);
                this.updateYourCoutScore($plugin, globalIndexWin);

                $resumeModalWindow.css({display: 'none'})

                startBtn.attr('disabled', true);

                this.fillCell($plugin, randomArray[0], timeOut);

                interval = setInterval(()=>{
                    let indexEl = randomArray[index++];

                    if(globalIndexWin >= 10 || globalIndexLose >= 9){
                        clearInterval(interval);
                    } else {
                        this.fillCell($plugin, indexEl, timeOut);
                    }
                }, timeOut)
                
            },
            destroy($plugin, yourScore, pcScore, status){
                let $gameFieldsCell = $plugin.find('.game-field-cell'),
                    $resumeModalWindow = $plugin.find('.result-game'),
                    $resumeTitle = $plugin.find('.resume'),

                startBtn = $plugin.find('.btn-start');
                startBtn.attr("disabled", false);

                if (status) {
                    $resumeTitle.html('You Win');
                    $resumeModalWindow.css({'background-color': '#cbff87'})
                } else {
                    $resumeTitle.html('You Lose');
                    $resumeModalWindow.css({'background-color': '#ff9f88'})
                }

                $resumeModalWindow.css({display: 'block'})

                $.each($gameFieldsCell, function(index, element){
                    $(element)
                        .css({'background-color': ''})
                        .removeClass('win lose')
                        .data('clicked', false);
                })
            },
            fillCell($plugin, index, timeOut){
                let $plygonItems = $plugin.find('.game-field-cell'),
                    $fillItem = $plygonItems.eq(index);

                new Promise(function(resolve, reject){
                    $fillItem.css({
                        'background-color': 'yellow'
                    });
                    resolve();
                }).then(()=>{
                    $fillItem.on('click', (event)=>{
                        let classList = event.currentTarget.classList;
    
                        if (classList.contains('win') || classList.contains('lose')) return;
    
                        globalIndexWin++;
                        
                        $fillItem
                            .css({'background-color': 'green'})
                            .addClass('win');
    
                        if (globalIndexWin == 10) {
                            this.destroy($plugin, globalIndexWin, globalIndexLose, true);
                        }
                        this.updateYourCoutScore($plugin, globalIndexWin);
    
                        $fillItem.data('clicked', true)
                    });

                    setTimeout(() => {
                        if ($fillItem.data('clicked')) return;
    
                        $fillItem
                            .css({'background-color': 'red'})
                            .addClass('lose');
    
                        setTimeout(function() {
                            globalIndexLose++;
                        }(globalIndexLose), 10);
    
                        this.updatePCCoutScore($plugin, globalIndexLose)
                        
                        if (globalIndexLose == 10) {
                            this.destroy($plugin, globalIndexWin, globalIndexLose, false);
                        }
    
                    }, timeOut);
                })
                
            },
            updatePCCoutScore($plugin, globalIndexLose){
                let $score = $plugin.find('.pc-score');

                return $score.html(globalIndexLose);  
            },
            updateYourCoutScore($plugin, globalIndexWin){
                let $score = $plugin.find('.your-score');

                return $score.html(globalIndexWin);  
            },
            getRandomArray(){
                let random = [],
                    max = 100;

                for(var i = 0; i < max; i++ ){
                    var temp = this.getRandomNumber(max);
                    if(random.indexOf(temp) == -1){
                        random.push(temp);
                    }
                    else
                        i--;
                }

                return random;
            },
            getRandomNumber(max){
                return Math.floor(Math.random() * max);
            }
        };

        let $this = this,
            $form = $this.find('.start-game');

        $form.on('submit', function(event){
            event.preventDefault();
            let data = $(this).serializeArray();
            if (data.length == 0) return alert('Enter time please');
            fn.init($this, this, data);
        })
    };
})(jQuery);
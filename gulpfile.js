'use strict';

const postcss = require('gulp-postcss')
const gulp = require('gulp');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');
const runSequence = require('run-sequence');
const rename = require('gulp-rename');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');

// css stuff
const styleSRC = './src/css/layout.css';
const styleDIST = './dist/css';
const styleWatch = './src/css/*.css'

// js stuff
const jsSRC = './src/js/index.js';
const jsDIST = './dist/js';
const jsWatch = './src/js/*.js';

const jsFILES = [jsSRC];


// Clean output directory
gulp.task('clean', () => del(['dist']));

// Task for generate css
gulp.task('styles', () => {

    const AUTOPREFIXER_BROWSERS = [
        'ie >= 11',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
      ];

    let plugins = [
        autoprefixer({browsers: AUTOPREFIXER_BROWSERS}),
        require('postcss-import')(),
        cssnano()
    ]

    return gulp.src(styleSRC)
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(rename('bundle.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(styleDIST));
})

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
    jsFILES.map(function(entry){
        return browserify({
          entries: [entry]
        })
        .transform(babelify, {presets: ['@babel/env']})
        .bundle()
        .pipe(source(entry))
        .pipe(rename('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(jsDIST))
    })
  });

// Gulp task to minify all files
gulp.task('default', ['clean'], function(){
  runSequence(
    'styles',
    'scripts'
  )
})

gulp.task('watch', ['default'], function(){
  gulp.watch(styleWatch, ['styles'])
  gulp.watch(jsWatch, ['scripts'])
})